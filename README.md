Adds Copy button to each market listing
which prefills market order form with
copied order's details

* Useful for creating orders for items
you don't have